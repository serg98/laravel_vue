<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function test(Request $request) {
        $this->validate($request, [
           'id' => 'required',
           'name' => 'required'
        ]);
        $user_data = [
            'id' => 14,
            'name' => 'John',
            'last_name' => 'Doe'
        ];
        return response()->json([
            'message' => 'Access Forbidden for you account',
        ], 200);
    }

    public function test2(Request $request) {
        return response()->json([
            'name' => 'Gago',
        ], 200);
    }
}
