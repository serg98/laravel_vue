window.onload = () => {
    // XML HTTP REQUEST .....................

    // const Http = new XMLHttpRequest();
    // const url='https://jsonplaceholder.typicode.com/posts';
    // Http.open("GET", url);
    // Http.send();
    // Http.onreadystatechange = (e) => {
    //     if(Http.readyState === 4) {
    //         console.log(JSON.parse(Http.responseText))
    //     }
    // }
};

// JQUERY AJAX .........
// $.ajax({
//     url: '/test',
//     method: 'POST',
//     data: {},
//     success: (data) => {
//         console.log(data)
//     },
//     error: (err) => {
//         console.error(err)
//     }
// });


//..............................................
function getData(url, method, data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: url,
            method: method,
            data: data,
            success: (data) => {
                resolve(data)
            },
            error: (err) => {
                reject(err)
            }
        });
    })
}

getData('/test', 'post', {id: 15}).then(res => {
   console.log(res)
}).catch(err => {
    if(err.status === 422) {
        let errors = JSON.parse(err.responseText).errors;
        if(errors.hasOwnProperty('name')) {
            document.write(errors.name[0])
        }
    }
});
//..............................................

$.ajax({
    method: 'get',
    url: '/test2',
    success: (data) => {
        let html = `<h1> Name: ${data.name}</h1>`;
        document.write(html)
    },
    error: (err) => {
        console.log(err)
    }
});


function shuffleArray(arr) {
    let tmp, current, top = arr.length;
    if(top) {
        while(--top) {
            current = Math.floor(Math.random() * (top + 1));
            tmp = arr[current];
            arr[current] = arr[top];
            arr[top] = tmp;
        }
    }
    return arr;
}

console.log(shuffleArray([1,8,7,5]));

