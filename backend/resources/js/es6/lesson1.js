// es6

// let and const
const A = 10;

let my_var = 'text';
//
// if(true) {
//     let x = 10;
//     console.log(A, my_var)
// }

// arrow functions

// let x = 1,
//     y = 2;

// let myFunction = () => x + y;
//
// console.log(myFunction());

const OBJ = {
    x: 15,
    y: 25,
    myFn() {
        let fn = () => {
            console.log('x', this.x);
            let fn2 = () => {
                console.log('y', this.y)
            };
            fn2()
        };
        fn();
    },

    destr({key1, key2, key3}, a) {
        console.log(key3)
    }
};

const var_obj = {key1: 1, key2: 2, key3: 3};

OBJ.destr(var_obj, 45);
// OBJ.myFn();

// destructuring
let {x, y, z} = {x: 1, y: 2, z: 3};

let arr1 = [1, 2, 3, 4];
let [a1, a2, a3] = arr1;

console.log(a3);

// spread
document.write(...arr1);

console.log(Math.max(...arr1));

// Promises

const success_request = false;

let myPromise = () => {
    return new Promise((resolve, reject) => {
        // fictive request
        if(success_request) {
            let data = {name: 'Gago'};
            setTimeout(()=> {
                resolve(data)
            }, 1500)
        }else {
            reject('data not found')
        }
    })
};

myPromise().then( (res) => {
    console.log(res);
}).catch( (err) => {
    console.log(err);
}).finally(() => {
    console.log('works in any cases')
});



