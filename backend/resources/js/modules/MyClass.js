import log from './myFunction.js'
export default class Snake {
    constructor(main_block, configs) {
        this.default_configs = {
            template: {
                cells_count: 40
            },
            speed: 500
        };
        this.configs = configs ? Object.assign(this.default_configs, configs) : this.default_configs;
        this.main_block = main_block;
        this.snake_coords = [];
        this.food_coords = [];
        this.game_over = false;
        this.directions = {
            up: 1,
            down: 3,
            left: 2,
            right: 4
        };
        this.current_direction = null;
    }

    static getCellDomElem(coords) {
        return document.querySelector(`[data-x="${coords.x}"][data-y="${coords.y}"]`)
    }

    get center() {
        let a = Math.floor(this.configs.template.cells_count / 2);
        return {x: a, y: a}
    }

    start() {
        if (!this.game_over){
            this.generateGameArea();
        }
        this.addToSnakeBody(this.center);
        this.drawSnake();
        this.addFood();
        this.addDomEvents();
    }

    generateGameArea() {
        let max = this.configs.template.cells_count;
        for (let i = 0; i < max; i++) {
            let row = document.createElement('div');
            row.classList.add('row');
            for (let j = 0; j < max; j++) {
                let cell = document.createElement('div');
                cell.classList.add('cell');
                cell.setAttribute('data-x', j);
                cell.setAttribute('data-y', i);
                row.appendChild(cell);
            }
            this.main_block.appendChild(row)
        }
    }

    addDomEvents() {
        document.addEventListener('keydown', (evt) => {
            switch (evt.code) {
                case ('ArrowUp'): this.move(this.directions.up);
                    break;
                case ('ArrowLeft'): this.move(this.directions.left);
                    break;
                case ('ArrowDown'): this.move(this.directions.down);
                    break;
                case ('ArrowRight'): this.move(this.directions.right);
                    break;
            }
        })
    }

    move(direction) {
        if(direction === this.current_direction) return false;
        this.current_direction = direction;
        switch (direction) {
            case(this.directions.up):
                this.runWithInterval('moveUp', this);
                break;
            case(this.directions.down):
                this.runWithInterval('moveDown', this);
                break;
            case (this.directions.left):
                this.runWithInterval('moveLeft', this);
                break;
            case (this.directions.right):
                this.runWithInterval('moveRight', this);
                break;
        }
    }

    runWithInterval(functionName, obj) {
        if(this.interval) clearInterval(this.interval);
        obj[functionName]();
        this.interval = setInterval(() => {
            obj[functionName]()
        }, this.configs.speed);
    }

    moveUp() {
        let head_coords = this.snake_coords[0],
            new_coords = {
                x: head_coords.x,
                y: head_coords.y - 1
            };
        this.nextStep(new_coords);
    };

    moveDown() {
        let head_coords = this.snake_coords[0],
            new_coords = {
                x: head_coords.x,
                y: head_coords.y + 1
            };
        this.nextStep(new_coords);
    }

    moveRight() {
        let head_coords = this.snake_coords[0],
            new_coords = {
                x: head_coords.x + 1,
                y: head_coords.y
            };
        this.nextStep(new_coords);
    }

    moveLeft() {
        let head_coords = this.snake_coords[0],
            new_coords = {
                x: head_coords.x - 1,
                y: head_coords.y
            };
        this.nextStep(new_coords);
    }

    addToSnakeBody(coords) {
        this.snake_coords.unshift(coords)
    }

    addFood(){
        let new_coords = {
            x: Math.floor(Math.random() * 10),
            y: Math.floor(Math.random() * 10)
        };
        let food_body = Snake.getCellDomElem(new_coords);
        if (food_body.classList.contains('body')){
            this.addFood();
        }
        else {
            this.addToFoodBody(new_coords);
        }
    }

    addToFoodBody(coords){
        this.food_coords.pop();
        this.food_coords.unshift(coords);
        this.food_coords.map((coords) => {
            let body_part = this.constructor.getCellDomElem(coords);
            body_part.classList.add('food');
        });
    }

    clear() {
        let snake_body_parts = this.main_block.querySelectorAll('.body');
        snake_body_parts.forEach((part) => {
            part.classList.remove('body');
            part.classList.remove('head');
        })
    }
    drawSnake() {
        this.snake_coords.map((coords, index) => {
            let body_part = this.constructor.getCellDomElem(coords);
            body_part.classList.add('body');
            if (index === 0) body_part.classList.add('head');
        })
    }

    gameOver(){
        this.food_coords = [];
        this.snake_coords = [];
        this.directions = {
            up: 1,
            down: 4,
            left: 2,
            right: 3
        };
        this.current_direction = null;
        this.game_over = true;
        this.start();
    }

    nextStep(coords) {
        let snake_body = this.constructor.getCellDomElem(coords);

        if (coords.x < 0 || coords.x > 39 || coords.y < 0 || coords.y > 39){
            alert('Game over!');
            let confirms = confirm('Do You want to restart game?');
            let body_part = this.constructor.getCellDomElem(this.food_coords[0]);
            body_part.classList.remove('food');
            if (confirms){
                this.gameOver()
            }
            return false;
        }
        if (snake_body.classList.contains('body')){
            alert('Game over!');
            let confirms = confirm('Do You want to restart game?');
            let body_part = this.constructor.getCellDomElem(this.food_coords[0]);
            body_part.classList.remove('food');
            if (confirms){
                this.gameOver()
            }
            return false;
        }
        this.addToSnakeBody(coords);
        this.snake_coords.pop();
        this.clear();
        this.drawSnake();
        if (this.food_coords[0].x === coords.x && this.food_coords[0].y === coords.y) {
            this.snake_coords.push(coords);
            let body_part = this.constructor.getCellDomElem(this.food_coords[0]);
            body_part.classList.remove('food');
            this.addFood();
        }
    }
}

