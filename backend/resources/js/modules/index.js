import config from "./config.js";
import Snake from "./MyClass.js";
const snakeGame = new Snake(document.getElementById('game-block'), config);

snakeGame.start();
