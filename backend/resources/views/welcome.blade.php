<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel Vue</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('./css/app.css')}}">
        <!-- Scripts -->
        <script type="text/javascript" src="{{ asset('./js/app.js') }}"></script>
        <script type="module" src="{{ asset('./js/modules/index.js') }}"></script>
    </head>
    <body>
	<div class='container'>
        	<div id="game-block"></div>
	</div>
    </body>
</html>
